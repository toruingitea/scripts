#!/bin/sh

# ▀█▀▄▀▄█▀▄█ █
#  █ ▀▄▀█▀▄▀▄█
#  simple, clean, elegant
#
# Page: https://toru.codeberg.page/
# Mastodon: @averagetiger89
# Repository: https://codeberg.org/toru
# Email: torupublic11@waifu.club

#              _                       _ __                _
#  __ _  _  _ | |_  ___  ___ _  _  ___| '_ \ ___  _ _   __| |
# / _` || || ||  _|/ _ \(_-/| || |(_-/| .__// -_)| ' \ / _` |
# \__/_| \_._| \__|\___//__/ \_._|/__/|_|   \___||_||_|\__/_|

##############################################################
# this script sets a 60 minutes spand (which you can change) #
# when it hits the limit (without useing your computer)	     #
# goes to an idle state, but first, it locks the screen	     #
##############################################################

pidof -s xautolock >&/dev/null
if [ $? -ne 0 ]; then
	xautolock -time 60 -locker "systemctl suspend" &
fi
