#!/bin/sh

# ▀█▀▄▀▄█▀▄█ █
#  █ ▀▄▀█▀▄▀▄█
#  simple, clean, elegant
#
# Page: https://toru.codeberg.page/
# Mastodon: @averagetiger89
# Repository: https://codeberg.org/toru
# Email: torupublic11@waifu.club
                        
#  _ __   ___  _ _   _  _ 
# | '  \ / -_)| ' \ | || |
# |_|_|_|\___||_||_| \_._|

# ¡SE ENCUENTRA EN FASE DE DESARROLLO!

show_menu(){
    normal=`echo "\033[m"`
    menu=`echo "\033[34m"`   #Blue
    number=`echo "\033[32m"` #Green
    bgred=`echo "\033[41m"`
    fgred=`echo "\033[31m"`
    printf "\n${menu}*********************************************${normal}\n"
    printf "${menu}»»${number} 1)${menu} Dmenupower.${normal}\n"
    printf "${menu}»»${number} 2)${menu} Dmenumount.${normal}\n"
    printf "${menu}»»${number} 3)${menu} Dmenuumount.${normal}\n"
    printf "${menu}»»${number} 4)${menu} rr.${normal}\n"
    printf "${menu}»»${number} 5)${menu} Mf.${normal}\n"
    printf "${menu}*********************************************${normal}\n"
    printf "Selecciona ${fgred}x para salir. ${normal}"
    read opt
}

option(){
    msgcolor=`echo "\033[01;31m"` # bold red
    normal=`echo "\033[00;00m"`   # normal white
    message=${@:-"${normal}Error: No message passed"}
    printf "${msgcolor}${message}${normal}\n"
}

clear
show_menu
while [ $opt != '' ]
    do
    if [ $opt = '' ]; then
      exit;
    else
      case $opt in
        1) clear;
            dmenupower;  
            show_menu;
        ;;
        2) clear;
            dmenumount ;
            show_menu;
        ;;
        3) clear;
            dmenuumount;
            show_menu;
        ;;
        4) clear;
            rr;
            show_menu;
        ;;
        5) clear;
            foot -e mf ;
            show_menu;
        ;;
        x)exit;
        ;;
        \n)exit;
        ;;
        *)clear;
            option "Error en la selección.";
            show_menu;
        ;;
      esac
    fi
done
