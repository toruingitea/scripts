#!/bin/sh

# ▀█▀▄▀▄█▀▄█ █
#  █ ▀▄▀█▀▄▀▄█
#  simple, clean, elegant
#
# Page: https://toru.codeberg.page/
# Mastodon: @averagetiger89
# Repository: https://codeberg.org/toru
# Email: torupublic11@waifu.club

#          __ 
#  _ __   / _|
# | '  \ |  _|
# |_|_|_||_|  
# _     _o _   (_o _  _| _  _
#||||_|_)|(_   | || )(_|(-`| 

#####################################################################################################################
#  sea donde estes, buscara mi directorio de musica para reproducir con mpv (el directorio se tiene que llamar ms)  #
# se necesita ffplay, find y fzf										    #
#####################################################################################################################

while true
do
   mpv $(find ~/ms -type f -not -path '*/\.git/*' | fzf --multi --layout=default --border --margin=3% --info=inline --prompt="Play Music: ")
done

# TODOS
## Crear un repositorio individual
## Crear wiki de como usarlo
## Agregar opcion de hacer playlist
## Agregar opcion de hacer una quenue
