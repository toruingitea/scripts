#!/bin/sh

# ▀█▀▄▀▄█▀▄█ █
#  █ ▀▄▀█▀▄▀▄█
#  simple, clean, elegant
#
# Page: https://toru.codeberg.page/
# Mastodon: @averagetiger89
# Repository: https://codeberg.org/toru
# Email: torupublic11@waifu.club

#        _            _   
#  __ _ | | ___  _ _ | |_ 
# / _` || |/ -_)| '_||  _|
# \__/_||_|\___||_|   \__|

########################################################################################
# este script esta hecho para usarse en conjunto con dunst (para las notificaciones)			       #
# para que las notificaciones tengan sonido. Asi que trata que tengas el mismo dunstrc #
########################################################################################

paplay ~/ms/sound/audios/noti.ogg
