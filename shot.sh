#!/bin/sh

# ▀█▀▄▀▄█▀▄█ █
#  █ ▀▄▀█▀▄▀▄█
#  simple, clean, elegant
#
# Page: https://toru.codeberg.page/
# Mastodon: @averagetiger89
# Repository: https://codeberg.org/toru
# Email: torupublic11@waifu.club

#      _          _
#  ___| |_   ___ | |_
# (_-/|   \ / _ \|  _|
# /__/|_||_|\___/ \__|

########################################################################
# # # my on screenshot utility using image magick		       #
# # it has 3 different modes; current window, selection and fullscreen #
########################################################################

print_date() {
	date '+%F_%T' | sed -e 's/:/-/g'
}

SCREENSHOTDIR="${HOME}/dl"
SCREENSHOTNAME="$(print_date).png"

note() {
	notify-send "screenshot name ${SCREENSHOTNAME}"
}

_end()
{
	note
	xdg-open "${SCREENSHOTNAME}"
	exit 0
}

region() { 
	killall unclutter
	import "${SCREENSHOTNAME}" 
	setsid unclutter &
	_end
}
window()
{
	import -window "$(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')" "${SCREENSHOTNAME}"
	_end
}
root()
{
	import -window root "${SCREENSHOTNAME}"
	_end
}

# Default Prompt For Selection
prompter(){
	case "$(printf 'a selected area\ncurrent window\nfull screen' | dmenu -l 6 -i -p 'Screenshot which area?')" in
		"a selected area") region ;;
		"current window") window ;;
		"full screen") root ;;
		*) exit ;;
	esac
_end
}

while getopts cwr: o; do
	case "$o" in
		c) region;;
		w) window ;;
		r) root ;;
		\?) printf 'Invalid option: -%s\n' "${o}" && exit ;;
	esac
done
prompter
