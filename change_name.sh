#!/bin/sh

# ▀█▀▄▀▄█▀▄█ █
#  █ ▀▄▀█▀▄▀▄█
#  simple, clean, elegant
#
# Page: https://toru.codeberg.page/
# Mastodon: @averagetiger89
# Repository: https://codeberg.org/toru
# Email: torupublic11@waifu.club

#      _                 __ _                                    
#  __ | |_   __ _  _ _  / _` | ___        _ _   __ _  _ __   ___ 
# / _||   \ / _` || ' \ \__. |/ -_)      | ' \ / _` || '  \ / -_)
# \__||_||_|\__/_||_||_||___/ \___|      |_||_|\__/_||_|_|_|\___|

###############################################################
# this script is about changing the name in a numerical order #
# of the files in the directory				      #
###############################################################

# Set the initial number
number=1

# Loop through each file in the current directory
for file in *; do
    # Check if the file is a regular file
    if [[ -f "$file" ]]; then
        # Get the file extension
        extension="${file##*.}"

        # Create the new filename
        new_filename="${number}.${extension}"

        # Rename the file
        mv "$file" "$new_filename"

        # Increment the number
        ((number++))
    fi
done
